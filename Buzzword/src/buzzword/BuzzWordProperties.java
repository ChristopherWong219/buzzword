package buzzword;

/**
 * @author Christopher Wong
 */
public enum BuzzWordProperties {
    WORKSPACE_HEADING_LABEL,
    ROOT_BORDERPANE_ID,
    TOP_TOOLBAR_ID,
    SEGMENTED_BUTTON_BAR,
    FIRST_TOOLBAR_BUTTON,
    LAST_TOOLBAR_BUTTON,
    HEADING_LABEL,
    MODE_LABEL,
    SIDE_TOOLBAR_ID;
}
