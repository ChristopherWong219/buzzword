package controller;

import gui.Workspace;

import java.util.ArrayList;

/**
 * Created by C on 12/11/2016.
 */
public class BuzzWordGridSolver {
    private ArrayList<String> wordsInBoard;
    private String[][] board;
    private int index;

    public BuzzWordGridSolver(String[][]board) {
        this.board = board;
        wordsInBoard = new ArrayList<String>();
    }

    public void solve(Workspace workspace){
        ArrayList<String> solverList = workspace.getSolverList();
        wordsInBoard = new ArrayList<String>();

        for(int i = 0; i < solverList.size(); i++){
            boolean found = find(solverList.get(i));
            if(found){
                if(!wordsInBoard.contains(solverList.get(i))) {
                    wordsInBoard.add(solverList.get(i));
                }
            }
        }
    }

    public boolean find(String word) {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (find(word, i, j)) {
                    return true;
                }
            }
        }
        return false;

    }

    private boolean find(String word, int index, int index2) {
        if (word.equals("")) {
            return true;
        }
        else if (index < 0 || index > 3 ||
                index2 < 0 || index2 > 3||
                !board[index][index2].equals(word.substring(0,1))) {
            return false;
        }
        else {
            String temp = board[index][index2];
            board[index][index2] = "";
            String substring = word.substring(1, word.length());
            boolean result = this.find(substring, index-1, index2-1) ||
                    this.find(substring, index-1,   index2) ||
                    this.find(substring, index-1, index2+1) ||
                    this.find(substring,   index, index2-1) ||
                    this.find(substring,   index, index2+1) ||
                    this.find(substring, index+1, index2-1) ||
                    this.find(substring, index+1,   index2) ||
                    this.find(substring, index+1, index2+1);
            board[index][index2] = temp;
            return result;
        }
    }

    public ArrayList<String> getWordsInBoard(){
        return wordsInBoard;
    }

}
