package controller;

import apptemplate.AppTemplate;
import data.GameData;
import gui.Workspace;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import propertymanager.PropertyManager;
import ui.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Random;

import static buzzword.BuzzWordProperties.MODE_LABEL;
import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_IMAGEDIR_PATH;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * @author Christopher Wong
 */
public class BuzzWordController implements FileController {
    private AppTemplate appTemplate; // shared reference to the application
    private GameData gamedata;    // shared reference to the game being played, loaded or saved
    private String category;
    private int totalSize;
    private Button lvl1;
    private Button lvl2;
    private Button lvl3;
    private Button lvl4;
    private int modeSelect;
    int[] lvlsUnlocked;
    ArrayList<Button> nodeButtons2;
    String[][] arrayOfLetters;
    ArrayList<String> wordsInBoard;
    Label timeRemaining;
    Integer STARTTIME = 30;
    Timeline timeline;
    IntegerProperty timeSeconds = new SimpleIntegerProperty(STARTTIME);
    TextField guessedLetters;
    ArrayList<Button> buttons;
    ArrayList<Button> hoveredOver;
    ArrayList<Button> hoveredOverChanged;
    String ourWord;
    ArrayList<String> ourWords;
    int totalScore = 0;
    Label total = new Label("Your score: ");
    ArrayList<String> valid = new ArrayList<String>();
    VBox rowSelectHolder;
    VBox wordsGotten;
    int wordPosTable = 0;
    int personalBest;
    int[] validPos;
    int currGridPos = 0;
    int currGridPosT = 0;
    ArrayList<Integer> validPosList = new ArrayList<Integer>();
    ArrayList<Button> duplicateList;
    ArrayList<Button> highlightedList;
    ArrayList<Integer> pathIndexes = new ArrayList<Integer>();
    ArrayList<ArrayList<Integer>> availPos = new ArrayList<ArrayList<Integer>>();
    boolean zeroChecked = false;
    Button replayLvl = new Button();
    Button startNext = new Button();
    private HBox topic;
    private Label modeLab;
    private String mode;
    boolean loggedIn = false;


    public BuzzWordController(AppTemplate appTemplate, Button gameButton) {

    }

    public BuzzWordController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
    }

    public void play(Workspace gameWorkspace) {
        buttons = gameWorkspace.getNodeButtons();
        hoveredOver = new ArrayList<Button>();
        hoveredOverChanged = new ArrayList<Button>();
        currGridPos = 0;
        currGridPosT = 0;
        zeroChecked = false;
        availPos.clear();
        pathIndexes.clear();
        ourWord = "";
        ourWords = new ArrayList<String>();
        gameWorkspace.getApp().getGUI().exitButtonGamePlay();
        valid.add("CORRECT");
        valid.add("WRONG");
        valid.add("ALREADY CHOSEN");
        wordsGotten.setMinSize(50, 100);
        for (int i = 0; i < wordsInBoard.size(); i++) {
            wordsGotten.getChildren().add(new Label(""));
        }

        for (int i = 0; i < buttons.size(); i++) {
            (buttons.get(i)).addEventHandler(MouseEvent.MOUSE_DRAGGED,
                    e -> {
                        for (int j = 0; j < gameWorkspace.getNodeButtons().size(); j++) {
                            if (duplicateList.contains((Button) gameWorkspace.getNodeButtons().get(j))) {
                                ((Button) gameWorkspace.getNodeButtons().get(j)).setId("button-round");
                            }
                        }

                        if (valid.contains(guessedLetters.getText())) {
                            guessedLetters.setText("");
                        }
                        try {
                            if (!hoveredOver.contains(((Button) e.getPickResult().getIntersectedNode())) &&
                                    buttons.contains(((Button) e.getPickResult().getIntersectedNode()))) {

                                if (currGridPos > 0) {
//                                        System.out.println(currGridPos);
                                    validPos = gameWorkspace.findNextPos(buttons.indexOf(hoveredOver.get(currGridPos - 1)));
                                    validPosList = new ArrayList<Integer>();
                                    for (int z = 0; z < validPos.length; z++) {
                                        validPosList.add(validPos[z]);
                                    }
                                }

                                if (currGridPos == 0 || validPosList.contains(buttons.indexOf((Button) e.getPickResult().getIntersectedNode()))) {
                                    hoveredOver.add(((Button) e.getPickResult().getIntersectedNode()));
                                    hoveredOverChanged.add(((Button) e.getPickResult().getIntersectedNode()));

                                    ourWord += (((Button) e.getPickResult().getIntersectedNode()).getText());
                                    guessedLetters.setText(guessedLetters.getText() +
                                            ((Button) e.getPickResult().getIntersectedNode()).getText());
                                    currGridPos++;
                                    e.getPickResult().getIntersectedNode().setId("orange");
                                }
                            }

//                            if (!hoveredOverChanged.contains(((Button) e.getPickResult().getIntersectedNode())) &&
//                                    buttons.contains(((Button) e.getPickResult().getIntersectedNode()))) {
//
//                                    hoveredOverChanged.add(((Button) e.getPickResult().getIntersectedNode()));
//
//                            }
                        } catch (Exception e1) {

                        }
                    });

            (buttons.get(i)).addEventHandler(MouseEvent.MOUSE_RELEASED,
                    e -> {
                        currGridPos = 0;
                        validPosList = new ArrayList<Integer>();
                        for (int i1 = 0; i1 < hoveredOver.size(); i1++) {
                            ((Button) hoveredOverChanged.get(i1)).setId("button-round");

                        }
                        reinitArrays();
                        for (int i2 = 0; i2 < wordsInBoard.size(); i2++) {
                            if (wordsInBoard.contains(ourWord) && !ourWords.contains(ourWord)) {
                                guessedLetters.setText("CORRECT");
                                totalScore += ourWord.length() * 10;
                                total.setText("Your Score: " + totalScore);
                                ((Label) wordsGotten.getChildren().get(wordPosTable)).setText(
                                        " " + ourWord + ": " + ourWord.length() * 10);
                                wordPosTable++;

                                break;
                            } else if (ourWords.contains(ourWord)) {
                                guessedLetters.setText("ALREADY CHOSEN");
                                break;
                            } else {
                                guessedLetters.setText("WRONG");
                                break;
                            }
                        }
                        ourWords.add(ourWord);
                        ourWord = "";
                    });
        }

        duplicateList = new ArrayList<Button>();
        highlightedList = new ArrayList<Button>();

        gameWorkspace.getFigurePane().setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.CONTROL) {
                gameWorkspace.getApp().getGUI().getSideBarPane().requestFocus();
            }

            else if (e.getText().matches("[a-z]")) {

                if (!zeroChecked) {
                    for (int i = 0; i < buttons.size(); i++) {
                        if (e.getText().equals(buttons.get(i).getText())) {
                            buttons.get(i).setId("orange");
                            pathIndexes.add(i); // current indexes we need to check
                            zeroChecked = true;
                        }
                    }
                }

                else {
                    for (int j = currGridPosT; j < pathIndexes.size(); j++) {
                        gameWorkspace.findNextPos(pathIndexes.get(j));
                        availPos.add(gameWorkspace.getAvailPos());
                    }
                    for(int i = 0; i < availPos.size(); i++){
                        for(int j = 0; j < buttons.size(); j++){
                            if(e.getText().equals(buttons.get(j).getText()) &&
                                    !availPos.get(i).contains(j)){
                            }
                            else if (e.getText().equals(buttons.get(j).getText())&&
                                    availPos.get(i).contains(j)){
                                buttons.get(j).setId("orange");
                                pathIndexes.add(i);
                                currGridPosT++;
                            }

                        }

                    }
                }



            }
            else if (e.getCode() == KeyCode.ENTER){
                for(int i = 0; i < highlightedList.size(); i++){
                    buttons.get(i).setId("button-round");
                }
                highlightedList.clear();
            }

        });


        if (gameWorkspace.getPauseButton().getTooltip().getText().equals("Pause game")) {
            gameWorkspace.getPauseButton().fire();
        }

    }

    private void reinitArrays() {
        hoveredOver = new ArrayList<Button>();
        hoveredOverChanged = new ArrayList<Button>();
    }


    @Override
    public void handleNewRequest() {

    }

    @Override
    public void handleSaveRequest() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        Path appDirPath = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
        Path targetPath = appDirPath.resolve(APP_WORKDIR_PATH.getParameter()
                + "/" + gamedata.getUsername() + ".json");

        try {
            appTemplate.getFileComponent().saveData(gamedata, targetPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleChangeRequest(String newUser, String newPass) throws IOException {

        final byte[] authBytes = newPass.getBytes(StandardCharsets.UTF_8);
        final String encoded = Base64.getEncoder().encodeToString(authBytes);

        PropertyManager propertyManager = PropertyManager.getManager();
        Path appDirPath = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
        Path targetPath = appDirPath.resolve(APP_WORKDIR_PATH.getParameter()
                + "/" + gamedata.getUsername() + ".json");

        String cutOldName = targetPath.toFile().toString().substring(0, targetPath.toFile().toString().length() - (gamedata.getUsername() + ".json").length());
        cutOldName = cutOldName + newUser + ".json";
        targetPath.toFile().renameTo(new File(cutOldName));

        targetPath = appDirPath.resolve(APP_WORKDIR_PATH.getParameter()
                + "/" + newUser + ".json");

        gamedata.setUsername(newUser);
        gamedata.setPassword(encoded);

        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.getApp().getGUI().setProfButton(newUser);

        try {
            appTemplate.getFileComponent().saveData(gamedata, targetPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void handleLoadRequest() throws IOException {

    }

    @Override
    public void handleExitRequest() {
        YesCancelDialogSingleton y = YesCancelDialogSingleton.getSingleton();
        y.show("Exit Application?", "Are you sure you want to exit the application?");
        String select = y.getSelection();

        if (select.equals("Yes"))
            System.exit(0);
    }

    @Override
    public void handleGameExitRequest() {
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        int timesClicked = gameWorkspace.getTimesClicked();

        if (timesClicked % 2 == 0) {
            gameWorkspace.changeButtons();
        }
        YesCancelDialogSingleton y = YesCancelDialogSingleton.getSingleton();
        y.show("Exit Application?", "Are you sure you want to exit the application?");
        String select = y.getSelection();
//
        if (select.equals("Yes")) {
            System.exit(0);
        } else if (timesClicked % 2 == 0) {
            gameWorkspace.changeButtons();
        }
//        } catch (IOException ioe) {

//        }

    }

    @Override
    public void handleHomeRequest() {
        YesCancelDialogSingleton a = YesCancelDialogSingleton.getSingleton();
        a.show("Go home", "Proceed to home page?");
        if (a.getSelection().equals("Yes")) {
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.goHome(gamedata.getUsername());
            gameWorkspace.clearMainWindow();
        }

    }

    @Override
    public void handleHomeGamePlayRequest() {
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        if (gameWorkspace.getPauseButton().getTooltip().getText().equals("Pause game")) {
            if (timeline != null) {
                timeline.pause();
            }
            gameWorkspace.getPauseButton().fire();
        }

        YesCancelDialogSingleton a = YesCancelDialogSingleton.getSingleton();
        a.show("Go home", "Proceed to home page?");
        if (a.getSelection().equals("Yes")) {
            if (timeline != null) {
                timeline.stop();
            }
            wordPosTable = 0;
            gameWorkspace.getPauseButton().fire();
            gameWorkspace.getPauseButton().setDisable(true);
            gameWorkspace.goHome(gamedata.getUsername());
            gameWorkspace.clearMainWindow();

        }
    }

    @Override
    public void handleHelpRequest() {
        HelpWindowSingleton h = HelpWindowSingleton.getSingleton();
        h.show();
    }

    @Override
    public void handleControlRequest() {
        Workspace gameWorkspace = (Workspace)appTemplate.getWorkspaceComponent();
        gameWorkspace.getApp().getGUI().getSideBarPane().setOnKeyReleased(new EventHandler<KeyEvent>() {
            final KeyCombination combo = new KeyCodeCombination(KeyCode.P, KeyCombination.CONTROL_DOWN);
            final KeyCombination combo2 = new KeyCodeCombination(KeyCode.L, KeyCombination.CONTROL_DOWN);
            final KeyCombination combo3 = new KeyCodeCombination(KeyCode.R, KeyCombination.CONTROL_DOWN);
            final KeyCode keycode = KeyCode.ALPHANUMERIC;
            public void handle(KeyEvent t) {
                if (combo.match(t)) {
                    if(!loggedIn){
                        handleCreateRequest();
                    }
                }
                else if(combo2.match(t)){
                    if(!loggedIn) {
                        handleLoginRequest();
                    }
                    else
                        handleLogout();
                }
                else if(combo3.match(t)){
                    if(timeline!=null && gameWorkspace.getPauseButton()!=null){

                        if(gameWorkspace.getPauseButton().getTooltip().getText().equals("Pause game")){
                            gameWorkspace.getPauseButton().fire();
                        }
                        YesNoCancelDialogSingleton a = YesNoCancelDialogSingleton.getSingleton();
                        a.show("Replay Level?", "Discard current game and replay level?");

                        if (a.getSelection().equals("Yes")) {
                            redoLevel();
                        }
                        else{

                        }
                    }
                }

                else if(t.getText().matches("[a-z]")){
                    gameWorkspace.getFigurePane().requestFocus();
                }
            }
        });
    }

    private void handleReplay() {
        PropertyManager propertyManager = PropertyManager.getManager();
        Workspace gameWorkspace = (Workspace)appTemplate.getWorkspaceComponent();
        gameWorkspace.getPauseButton().fire();
        timeline.pause();


        topic = new HBox();

        modeLab = new Label("Topic: " + mode);

        modeLab.getStyleClass().setAll(propertyManager.getPropertyValue(MODE_LABEL));

        topic.getChildren().add(modeLab);
        topic.setAlignment(Pos.CENTER);

        gameWorkspace.getFigurePane().setTop(topic);
    }

    public void handleCreateRequest() {
        CreateProfileDialogSingleton c = CreateProfileDialogSingleton.getSingleton();
        c.show("Create a Profile!", "Create a username and password.");
        if (c.getSelection() != null && c.getSelection().equals("Create")) {
            gamedata = (GameData) (appTemplate.getDataComponent());
            gamedata.init();

            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();

            gameWorkspace.reinitializeCL(c.getUsername());

            String username = c.getUsername();
            String password = c.getPassword();

            final byte[] authBytes = password.getBytes(StandardCharsets.UTF_8);
            final String encoded = Base64.getEncoder().encodeToString(authBytes);

            int[] levelsUnlocked = {1, 1, 1};
            int personalBest = 0;

            gamedata.setUsername(username);
            gamedata.setPassword(encoded);
            gamedata.setLevelsUnlocked(levelsUnlocked);
            gamedata.setPersonalBest(personalBest);

            c.reset();

            try {
                handleSaveRequest();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else{
            loggedIn = false;
        }
    }

    public void handleLoginRequest() {
        gamedata = (GameData) (appTemplate.getDataComponent());
        gamedata.init();

        LoginProfileDialogSingleton c = LoginProfileDialogSingleton.getSingleton();
        c.show("Login", "Login with a username and password.");

        if (c.getSelection().equals("Login")) {

            loggedIn = true;

            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitializeCL(c.getUsernameData());
            gamedata.setUsername(c.getUsernameData());

            final byte[] authBytes = c.getPasswordData().getBytes(StandardCharsets.UTF_8);
            final String encoded = Base64.getEncoder().encodeToString(authBytes);
            gamedata.setPassword(encoded);

            gamedata.setLevelsUnlocked(c.getUnlocked());
            gamedata.setPersonalBest(c.getPersonalBest());

        }
        else
            loggedIn = false;

    }

    @Override
    public void handleComboRequest() {
//        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
//        gameWorkspace.getLvlSelectionScreen();
    }

    @Override
    public void handleStartRequest() {
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        setLvlsUnlocked(gamedata.getLevelsUnlocked());
        getLvlSelectionScreen(gameWorkspace);
    }

    public void setLvlsUnlocked(int[] lvlsUnlocked) {
        this.lvlsUnlocked = lvlsUnlocked;
    }

    public void getLvlSelectionScreen(Workspace gameWorkspace) {
        mode = gameWorkspace.getApp().getGUI().getCombo();
        if (mode.contains("Select Mode")) {
            AppMessageDialogSingleton select = AppMessageDialogSingleton.getSingleton();
            select.show("Nothing Selected!", "Please select something from the drop down first!");
        } else {

            gameWorkspace.getApp().getGUI().transitionFromCombo();
            gameWorkspace.getApp().getGUI().createSelectionScreen();

            gameWorkspace.getApp().getGUI().exitHomeRegular();


            PropertyManager propertyManager = PropertyManager.getManager();

            topic = new HBox();

            modeLab = new Label("Topic: " + mode);

            modeLab.getStyleClass().setAll(propertyManager.getPropertyValue(MODE_LABEL));

            topic.getChildren().add(modeLab);
            topic.setAlignment(Pos.CENTER);

            gameWorkspace.getFigurePane().setTop(topic);

            gameWorkspace.initSelect();

            lvl1 = new Button("1");
            lvl2 = new Button("2");
            lvl3 = new Button("3");
            lvl4 = new Button("4");

            category = "";
            totalSize = 0;

            if (mode.equals("Food")) {
                modeSelect = 0;
                category = "food.txt";
                totalSize = 54;
            } else if (mode.equals("Christmas")) {
                modeSelect = 1;
                category = "christmas.txt";
                totalSize = 72;
            } else if (mode.equals("Fitness")) {
                modeSelect = 2;
                category = "fitness.txt";
                totalSize = 68;
            }

            int unlocked = lvlsUnlocked[modeSelect];

            nodeButtons2 = new ArrayList<Button>();
            nodeButtons2.add(lvl1);
            nodeButtons2.add(lvl2);
            nodeButtons2.add(lvl3);
            nodeButtons2.add(lvl4);

            for (int i = unlocked; i < nodeButtons2.size(); i++) {
                (nodeButtons2.get(i)).setDisable(true);
            }

            gameWorkspace.initButtonStyle(nodeButtons2);
            initLvlSelect(nodeButtons2, gameWorkspace);

            gameWorkspace.getRowOneSelect().getChildren().addAll(lvl1, lvl2, lvl3, lvl4);
            gameWorkspace.getRowOneSelect().setAlignment(Pos.CENTER);

            rowSelectHolder = new VBox();

            rowSelectHolder.getChildren().add(gameWorkspace.getRowOneSelect());
            gameWorkspace.getFigurePane().setCenter(rowSelectHolder);
        }
    }

    private void initLvlSelect(ArrayList<Button> nodeButtons2, Workspace gameWorkspace) {
        gameWorkspace.getApp().getGUI().removeHelp();
        for (int i = 0; i < nodeButtons2.size(); i++) {
            int finalI = i;
            nodeButtons2.get(i).setOnAction(event -> {
                gameWorkspace.getApp().getGUI().exitHomeGamePlay();
                initGameplayScreen(finalI, gameWorkspace, false);
            });
        }
    }

    private void initGameplayScreen(int i, Workspace gameWorkspace, boolean replay) {
        rowSelectHolder = new VBox();
        rowSelectHolder.getChildren().addAll(gameWorkspace.getRowOne());
        rowSelectHolder.getChildren().add(gameWorkspace.getRowTwo());
        rowSelectHolder.getChildren().add(gameWorkspace.getRowThree());
        rowSelectHolder.getChildren().add(gameWorkspace.getRowFour());
        gameWorkspace.disable(gameWorkspace.getNodeButtons(), false);

        gameWorkspace.getPossible(category, totalSize, i);

        String word = gameWorkspace.getWord(i, replay);
        if (replay) {
            PropertyManager propertyManager = PropertyManager.getManager();

            gameWorkspace.getPauseButton().fire();
            timeline.pause();


            topic = new HBox();

            modeLab = new Label("Topic: " + mode);

            modeLab.getStyleClass().setAll(propertyManager.getPropertyValue(MODE_LABEL));

            topic.getChildren().add(modeLab);
            topic.setAlignment(Pos.CENTER);

            gameWorkspace.getFigurePane().setTop(topic);
        }

        char[] wordArray = word.toCharArray();

        Random random = new Random();
        int beginLetterPos = random.nextInt(gameWorkspace.getNodeButtons().size()); // 0 - 15

        ((Button) gameWorkspace.getNodeButtons().get(beginLetterPos)).setText(wordArray[0] + ""); // set 1st letter of word to button at pos

        int[] posNextPositions = gameWorkspace.findNextPos(beginLetterPos);
        int randomNextIndex = random.nextInt(posNextPositions.length);
        int nextLetterPos = posNextPositions[randomNextIndex];

        for (int k = 1; k < wordArray.length; k++) {

            while (!(((Button) gameWorkspace.getNodeButtons().get(nextLetterPos)).getText().equals(""))) {
                randomNextIndex = random.nextInt(posNextPositions.length);
                nextLetterPos = posNextPositions[randomNextIndex];
            }

            ((Button) gameWorkspace.getNodeButtons().get(nextLetterPos)).setText(wordArray[k] + "");

            posNextPositions = gameWorkspace.findNextPos(nextLetterPos);
            randomNextIndex = random.nextInt(posNextPositions.length);
            nextLetterPos = posNextPositions[randomNextIndex];

        }

        char[] alphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();

        Random random2 = new Random();

        for (int j = 0; j < gameWorkspace.getNodeButtons().size(); j++) {
            if (((Button) gameWorkspace.getNodeButtons().get(j)).getText().equals("")) {
                ((Button) gameWorkspace.getNodeButtons().get(j)).setText(alphabet[random2.nextInt(26)] + "");
            }
        }

        gameWorkspace.getApp().getGUI().exitButtonGamePlay();
        gameWorkspace.getFigurePane().setCenter(rowSelectHolder);


        PropertyManager propertyManager = PropertyManager.getManager();
        gameWorkspace.initPauseButton();

        Tooltip pauseTip = new Tooltip(propertyManager.getPropertyValue(PAUSE_TOOLTIP.toString()));
        gameWorkspace.getPauseButton().setTooltip(pauseTip);

        URL imgDirURL = AppTemplate.class.getClassLoader().getResource(APP_IMAGEDIR_PATH.getParameter());
        String icon = PAUSE_ICON.toString();
        if (imgDirURL == null)
            try {
                throw new FileNotFoundException("Image resources folder does not exist.");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        try {
            try (InputStream imgInputStream = Files.newInputStream(Paths.get(imgDirURL.toURI()).resolve(propertyManager.getPropertyValue(icon)))) {
                Image buttonImage = new Image(imgInputStream);
                gameWorkspace.getPauseButton().setGraphic(new ImageView(buttonImage));
            } catch (URISyntaxException e) {
                e.printStackTrace();
                System.exit(1);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        gameWorkspace.getPauseButton().setOnAction(event -> {
            if (gameWorkspace.getTimesClicked() % 2 == 0) { // if even, change button from pause to play

                Tooltip playTip = new Tooltip(propertyManager.getPropertyValue(PLAY_TOOLTIP.toString()));
                gameWorkspace.getPauseButton().setTooltip(playTip);
                timeline.pause();

                for (int j = 0; j < buttons.size(); j++) {
                    buttons.get(j).setDisable(true);
                }

                URL imgDirURL2 = AppTemplate.class.getClassLoader().getResource(APP_IMAGEDIR_PATH.getParameter());
                String icon2 = PLAY_ICON.toString();
                if (imgDirURL2 == null)
                    try {
                        throw new FileNotFoundException("Image resources folder does not exist.");
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                try {
                    try (InputStream imgInputStream = Files.newInputStream(Paths.get(imgDirURL.toURI()).resolve(propertyManager.getPropertyValue(icon2)))) {
                        Image buttonImage = new Image(imgInputStream);
                        gameWorkspace.getPauseButton().setGraphic(new ImageView(buttonImage));
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                        System.exit(1);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                gameWorkspace.setTimesClicked(gameWorkspace.getTimesClicked() + 1);

                for (int j = 0; j < gameWorkspace.getNodeButtons().size(); j++) {
                    ((Button) gameWorkspace.getNodeButtons().get(j)).setId("button-pause");
                }
            } else {   // odd, change button from play to pause

                Tooltip pauseTip2 = new Tooltip(propertyManager.getPropertyValue(PAUSE_TOOLTIP.toString()));
                gameWorkspace.getPauseButton().setTooltip(pauseTip2);
                timeline.play();
                for (int j = 0; j < buttons.size(); j++) {
                    buttons.get(j).setDisable(false);
                }

                URL imgDirURL2 = AppTemplate.class.getClassLoader().getResource(APP_IMAGEDIR_PATH.getParameter());
                String icon2 = PAUSE_ICON.toString();
                if (imgDirURL2 == null)
                    try {
                        throw new FileNotFoundException("Image resources folder does not exist.");
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                try {
                    try (InputStream imgInputStream = Files.newInputStream(Paths.get(imgDirURL2.toURI()).resolve(propertyManager.getPropertyValue(icon2)))) {
                        Image buttonImage = new Image(imgInputStream);
                        gameWorkspace.getPauseButton().setGraphic(new ImageView(buttonImage));
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                        System.exit(1);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                gameWorkspace.setTimesClicked(gameWorkspace.getTimesClicked() + 1);

                for (int j = 0; j < gameWorkspace.getNodeButtons().size(); j++) {
                    ((Button) gameWorkspace.getNodeButtons().get(j)).setId("button-round");
                    if (highlightedList.contains((Button) gameWorkspace.getNodeButtons().get(j))) {
                        ((Button) gameWorkspace.getNodeButtons().get(j)).setId("orange");
                    }
                }


            }
        });


        VBox pauseBox = new VBox();
        HBox timerStuff = new HBox();
        VBox timerStuff2 = new VBox(3);
        wordsGotten = new VBox();


        wordPosTable = 0;
        Label timeLabel = new Label("Time remaining: ");
        totalScore = 0;
        total.setText("Your Score: " + totalScore);
        timeRemaining = new Label();
        timeRemaining.textProperty().bind(timeSeconds.asString());
        if (timeline != null) {
            timeline.stop();
        }

        STARTTIME = gameWorkspace.getTargetScore(); // i set starttime in seconds to targetScore, they're the same
//        STARTTIME = 500; // i set starttime in seconds to targetScore, they're the same
        timeSeconds.set(STARTTIME);
        timeline = new Timeline();
        timeline.getKeyFrames().add(
                new KeyFrame(Duration.seconds(STARTTIME + 1),
                        new KeyValue(timeSeconds, 0)));
        timeline.playFromStart();
        timeline.pause();
        timeline.setOnFinished(e -> {
            timeline.pause();
            String words = "";
            words = words + "\n";
            if (totalScore >= gameWorkspace.getTargetScore()) {
                unlockNextLevel();
                words += "Congrats! You won! Target score was " + "\n" +
                        +gameWorkspace.getTargetScore() + ", and you scored " + totalScore + "." + "\n" + "\n";
            } else {
                words += "You lose! Target score was " + "\n" +
                        +gameWorkspace.getTargetScore() + ", and you scored " + totalScore + "." + "\n" + "\n";
            }

            words = words + "Words in the board:";
            for (int k = 0; k < wordsInBoard.size(); k++) {

                words = words + "\n";
                words = words + wordsInBoard.get(k);
            }
            String finalWords = words;
//            Platform.runLater(()->{
            AppMessageDialogSingleton b = AppMessageDialogSingleton.getSingleton();
            b.setContents("", "Timer has reached 0, game has ended.");
            b.concaetenateMessage(finalWords);
            b.showDialog();
//                });

            gameWorkspace.getPauseButton().setDisable(true);

            ArrayList<Button> buttons = gameWorkspace.getNodeButtons();
            for (int j = 0; j < buttons.size(); j++) {
                buttons.get(j).setDisable(true);
            }
        });

        timerStuff.getChildren().addAll(timeLabel, timeRemaining);
        timerStuff2.getChildren().add(timerStuff);
        guessedLetters = new TextField();
        guessedLetters.setPadding(new Insets(10, 10, 10, 10));
        guessedLetters.setEditable(false);
        timerStuff2.getChildren().add(guessedLetters);
        wordsGotten.setId("wordsGotten");
        timerStuff2.getChildren().add(wordsGotten);
        timerStuff2.getChildren().add(total);
        timerStuff2.getChildren().add(gameWorkspace.getTarget());
        timerStuff2.setId("timer");
        replayLvl = new Button("Replay Level");
        startNext = new Button("Start Next");

        initButtons();

        timerStuff2.getChildren().add(replayLvl);
        timerStuff2.getChildren().add(startNext);


        Label level = new Label("Level: " + (i + 1));
        pauseBox.getChildren().add(level);
        pauseBox.getChildren().add(gameWorkspace.getPauseButton());
        pauseBox.setAlignment(Pos.CENTER);
        gameWorkspace.getFigurePane().setBottom(pauseBox);
        gameWorkspace.getFigurePane().setRight(timerStuff2);

        int count = 0;
        arrayOfLetters = new String[4][4];
        for (int k = 0; k < arrayOfLetters.length; k++) {
            for (int l = 0; l < arrayOfLetters.length; l++) {
                arrayOfLetters[k][l] = ((Button) gameWorkspace.getNodeButtons().get(count)).getText();
                count++;
            }
        }

    BuzzWordGridSolver b = new BuzzWordGridSolver(arrayOfLetters);
    b.solve(gameWorkspace);
        System.out.println("a");
    wordsInBoard = b.getWordsInBoard();
        for(int z = 0; z <wordsInBoard.size(); z++){
            System.out.println(wordsInBoard.get(z));
        }
    play(gameWorkspace);

}
    private void initButtons() {
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        replayLvl.setOnAction(e -> {
            if (timeline != null) {
                timeline.pause();
            }
            if (gameWorkspace.getPauseButton().getTooltip().getText().equals("Pause game")) {
                gameWorkspace.getPauseButton().fire();
            }
            YesNoCancelDialogSingleton a = YesNoCancelDialogSingleton.getSingleton();
            a.show("Replay Level?", "Discard current game and replay level?");

            if (a.getSelection().equals("Yes")) {
                redoLevel();
            }

        });

        int[]arr = gamedata.getLevelsUnlocked();
        int index = modeSelect;
        int currLvl = gameWorkspace.getTargetScore()/10 - 2;
        if(arr[index] == currLvl){
            startNext.setDisable(true);
        }

        startNext.setOnAction(e -> {
            if (timeline != null) {
                timeline.pause();
            }
            if (gameWorkspace.getPauseButton().getTooltip().getText().equals("Pause game")) {
                gameWorkspace.getPauseButton().fire();
            }
            YesNoCancelDialogSingleton y = YesNoCancelDialogSingleton.getSingleton();
            y.show("Start Next level", "Proceed to next level?");
            if (y.getSelection().equals("Yes")) {
                startNext();
            }
        });
    }

    private void redoLevel() {
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.goHome(gamedata.getUsername());
        gameWorkspace.clearMainWindow();
        gameWorkspace.getApp().getGUI().removeHelp();
        gameWorkspace.getApp().getGUI().removeCombo();
        gameWorkspace.getApp().getGUI().removeStart();
        gameWorkspace.getApp().getGUI().addHome();
        gameWorkspace.getApp().getGUI().exitHomeGamePlay();
        initGameplayScreen((gameWorkspace.getTargetScore()/10-3), gameWorkspace, true);

    }

    private void startNext() {
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.goHome(gamedata.getUsername());
        gameWorkspace.clearMainWindow();
        gameWorkspace.getApp().getGUI().removeHelp();
        gameWorkspace.getApp().getGUI().removeCombo();
        gameWorkspace.getApp().getGUI().removeStart();
        gameWorkspace.getApp().getGUI().addHome();
        gameWorkspace.getApp().getGUI().exitHomeGamePlay();
        initGameplayScreen((gameWorkspace.getTargetScore()/10-2), gameWorkspace, false);

    }


    private void unlockNextLevel() {
        int[] arr = gamedata.getLevelsUnlocked();
        int currentLvl = 0;

        Workspace gameWorkspace = (Workspace)appTemplate.getWorkspaceComponent();
        if(gameWorkspace.getTargetScore() == 30){
            currentLvl = 1;
        }
        else if(gameWorkspace.getTargetScore() == 40){
            currentLvl = 2;
        }
        else if(gameWorkspace.getTargetScore() == 50){
            currentLvl = 3;
        }
        else if(gameWorkspace.getTargetScore() == 60){
            currentLvl = 4;
        }

        if(currentLvl == arr[modeSelect] && currentLvl != 4)
        {
            currentLvl++;
            arr[modeSelect] = currentLvl;
            gamedata.setLevelsUnlocked(arr);
            startNext.setDisable(false);
        }


        personalBest = gamedata.getPersonalBest();
        if(totalScore > personalBest){
            Platform.runLater(()->{;
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("New High Score!");
                alert.setContentText("Congrats! You beat your personal best score of" + "\n" +
                        personalBest + " with a score of " + totalScore);
                alert.show();
            });
            gamedata.setPersonalBest(totalScore);

        }

        try {
            handleSaveRequest();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleLogout(){
        Workspace gameWorkspace = (Workspace)appTemplate.getWorkspaceComponent();
        if(timeline!=null){
            timeline.pause();
        }
        if(gameWorkspace.getPauseButton() != null && gameWorkspace.getPauseButton().getTooltip().getText().equals("Pause game")){
            gameWorkspace.getPauseButton().fire();
        }

        YesCancelDialogSingleton y = YesCancelDialogSingleton.getSingleton();
        y.show("Logout?", "Log out of your account?");

        if(y.getSelection().equals("Yes")){
            loggedIn = false;
            gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize(appTemplate);
            gamedata = null;
            wordPosTable = 0;
            if(timeline != null) {
                timeline.stop();

            }

        }
        else {

        }
    }

    @Override
    public void handleViewProfile() {
        if(timeline!=null){
            timeline.pause();
        }
        Workspace gameWorkspace = (Workspace)appTemplate.getWorkspaceComponent();
        if(gameWorkspace.getPauseButton() != null && gameWorkspace.getPauseButton().getTooltip().getText().equals("Pause game")){
            gameWorkspace.getPauseButton().fire();
        }
        ViewProfileDialogSingleton v = ViewProfileDialogSingleton.getSingleton();
        v.show("Your Profile!", "Welcome to your profile, " + gamedata.getUsername() + "!");

        if(v.getSelection().equals("Logout")){
            loggedIn = false;
            gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize(appTemplate);
            gamedata = null;
            wordPosTable = 0;
            if(timeline != null) {
                timeline.stop();

            }

        }
        else if (v.getSelection().equals("Edit")){
            EditProfileDialogSingleton e = EditProfileDialogSingleton.getSingleton();
            e.show("Edit Your Profile!", "");

            if(e.getSelection().equals("Edit")){
                try {
                    handleChangeRequest(e.getUsername(), e.getPassword());
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

            }
        }
    }
}
