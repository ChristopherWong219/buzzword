package controller;

/**
 * @author Christopher Wong
 */
public class GameError extends Error {

    public GameError(String message) {
        super(message);
    }
}
