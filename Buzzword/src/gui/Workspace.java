package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.BuzzWordController;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import propertymanager.PropertyManager;
import ui.AppGUI;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.stream.Stream;

import static buzzword.BuzzWordProperties.*;

/**
 * @author Christopher Wong
 */
public class Workspace extends AppWorkspaceComponent {

    AppTemplate app;        // the actual application
    AppGUI gui;             // the GUI inside which the application sits
    Label guiHeadingLabel;  // GUI heading label ("!BuzzWord!")
    HBox headPane;          // container to display the heading label
    VBox bodyPane;          // container to display the main game
    BorderPane figurePane;  // container to display the BuzzWord nodes
    VBox rowHolder;         // holds the rows of buttons for buzzword
    HBox rowOne;            // top
    HBox rowTwo;            // second to last from top
    HBox rowThree;          // second to last from bottom
    HBox rowFour;           // bottom
    BuzzWordController controller;    // controller for BuzzWord application
    ArrayList nodeButtons;      // arraylist of our buttons
    Button one, two, three, four, five, six, seven, eight, nine, ten, eleven, twelve, thirteen, fourteen, fifteen,
            sixteen;

    HBox rowOneSelect;
    HBox rowTwoSelect;
    Button pause;
    int timesClicked = 0;
    Label target = new Label("Target: ");
    int targetScore = 0;
    HashSet<String> possibleWords = new HashSet<String>(); // used to search if word player selects is contained
    ArrayList<String> possibleWords2 = new ArrayList<String>(); // list of words from file we'll read from
    ArrayList<String> solverList = new ArrayList<String>();      // list of words used by the solver method
    ArrayList<Integer> availPositions;

    public Workspace(AppTemplate initApp) throws IOException {
        app = initApp;
        gui = app.getGUI();
        app.getGUI().getPrimaryScene().setOnKeyReleased(e->{
            if(e.getCode() == KeyCode.CONTROL) {
                app.getGUI().getSideBarPane().requestFocus();
                app.getGUI().getFileController().handleControlRequest();
            }
        });

        controller = (BuzzWordController) gui.getFileController();    //new HangmanController(app, startGame); <-- THIS WAS A MAJOR BUG!??
        layoutGUI();     // initialize all the workspace (GUI) components including the containers and their layout

    }

    public void layoutGUI() {
        PropertyManager propertyManager = PropertyManager.getManager();
        guiHeadingLabel = new Label(propertyManager.getPropertyValue(WORKSPACE_HEADING_LABEL));

        headPane = new HBox();
        headPane.getChildren().add(guiHeadingLabel);
        headPane.setAlignment(Pos.CENTER);

        bodyPane = new VBox();
        figurePane = new BorderPane();
        workspace = new VBox();

        layoutHome();

        workspace.getChildren().addAll(headPane, bodyPane);
    }

    public void layoutHome() {
        nodeButtons = new <Button>ArrayList();

        one = new Button("");
        two = new Button("");
        three = new Button("");
        four = new Button("");
        five = new Button("");
        six = new Button("");
        seven = new Button("");
        eight = new Button("");
        nine = new Button("");
        ten = new Button("");
        eleven = new Button("");
        twelve = new Button("");
        thirteen = new Button("");
        fourteen = new Button("");
        fifteen = new Button("");
        sixteen = new Button("");

        nodeButtons.add(one);
        nodeButtons.add(two);
        nodeButtons.add(three);
        nodeButtons.add(four);
        nodeButtons.add(five);
        nodeButtons.add(six);
        nodeButtons.add(seven);
        nodeButtons.add(eight);
        nodeButtons.add(nine);
        nodeButtons.add(ten);
        nodeButtons.add(eleven);
        nodeButtons.add(twelve);
        nodeButtons.add(thirteen);
        nodeButtons.add(fourteen);
        nodeButtons.add(fifteen);
        nodeButtons.add(sixteen);

        initButtonStyle(nodeButtons);

        disable(nodeButtons, true);

        rowOne = new HBox();
        rowOne.setSpacing(10);
        rowOne.setPadding(new Insets(10, 10, 10, 10));
        rowOne.setAlignment(Pos.CENTER);
        rowOne.getChildren().addAll(one, two, three, four);

        rowTwo = new HBox();
        rowTwo.setSpacing(10);
        rowTwo.setPadding(new Insets(10, 10, 10, 10));
        rowTwo.setAlignment(Pos.CENTER);
        rowTwo.getChildren().addAll(five, six, seven, eight);

        rowThree = new HBox();
        rowThree.setSpacing(10);
        rowThree.setPadding(new Insets(10, 10, 10, 10));
        rowThree.setAlignment(Pos.CENTER);
        rowThree.getChildren().addAll(nine, ten, eleven, twelve);

        rowFour = new HBox();
        rowFour.setSpacing(10);
        rowFour.setPadding(new Insets(10, 10, 10, 10));
        rowFour.setAlignment(Pos.CENTER);
        rowFour.getChildren().addAll(thirteen, fourteen, fifteen, sixteen);

        rowHolder = new VBox();
        rowHolder.getChildren().addAll(rowOne, rowTwo, rowThree, rowFour);


        figurePane.setCenter(rowHolder);


        bodyPane.setAlignment(Pos.CENTER);
        bodyPane.getChildren().add(figurePane);

        HBox blankBoxLeft = new HBox();
        HBox blankBoxRight = new HBox();
        HBox.setHgrow(blankBoxLeft, Priority.ALWAYS);
        HBox.setHgrow(blankBoxRight, Priority.ALWAYS);
    }

    public void initButtonStyle(ArrayList nodeButtons) {
        for (int i = 0; i < nodeButtons.size(); i++) {
            ((Button) nodeButtons.get(i)).setId("button-round");
        }
    }

    /**
     * This function specifies the CSS for all the UI components known at the time the workspace is initially
     * constructed. Components added and/or removed dynamically as the application runs need to be set up separately.
     */
    @Override
    public void initStyle() {
        PropertyManager propertyManager = PropertyManager.getManager();

        gui.getAppPane().setId(propertyManager.getPropertyValue(ROOT_BORDERPANE_ID));

        gui.getToolbarPane().getStyleClass().setAll(propertyManager.getPropertyValue(SEGMENTED_BUTTON_BAR));
        gui.getToolbarPane().setId(propertyManager.getPropertyValue(TOP_TOOLBAR_ID));

        gui.getSideBarPane().getStyleClass().setAll(propertyManager.getPropertyValue(SEGMENTED_BUTTON_BAR));
        gui.getSideBarPane().setId(propertyManager.getPropertyValue(SIDE_TOOLBAR_ID));

        gui.getMainGamePane().getStyleClass().setAll(propertyManager.getPropertyValue(SEGMENTED_BUTTON_BAR));
        gui.getMainGamePane().setId(propertyManager.getPropertyValue(SIDE_TOOLBAR_ID));

        ObservableList<Node> toolbarChildren = gui.getSideBarPane().getChildren();
        toolbarChildren.get(0).getStyleClass().add(propertyManager.getPropertyValue(FIRST_TOOLBAR_BUTTON));
        toolbarChildren.get(toolbarChildren.size() - 1).getStyleClass().add(propertyManager.getPropertyValue(LAST_TOOLBAR_BUTTON));

        workspace.getStyleClass().add(CLASS_BORDERED_PANE);
        guiHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL));
        app.getWorkspaceComponent().activateWorkspace(app.getGUI().getAppPane());
    }

    public void reinitialize(AppTemplate appTemplate) {
        try {
            app.getGUI().initializeBars();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            app.getGUI().initializeBarHandlers(appTemplate);
        } catch (InstantiationException e) {
            e.printStackTrace();
        }

        clearMainWindow();

    }

    public void clearMainWindow() {
        try {
            app.getGUI().reinitializeWindow(); // start the app window (without the application-specific workspace)
        } catch (IOException e) {
            e.printStackTrace();
        }
        app.initStylesheet();
        initStyle();

        bodyPane.getChildren().remove(figurePane);
        figurePane = new BorderPane();
        layoutHome();
    }

    public void reinitializeCL(String username) {
        app.getGUI().transitionFromLogin();
        app.getGUI().addProfButton(username);
    }

    public void goHome(String username){
        app.getGUI().refreshSidebar();
        app.getGUI().addProfButton(username);

        reinitBody();


    }

    public int[] findNextPos(int beginLetterPos) {
        availPositions = new ArrayList<Integer>();

        if(beginLetterPos == 0){
            availPositions.add(4);
            availPositions.add(5);
            availPositions.add(1);
        }
        else if (beginLetterPos == 3){
            availPositions.add(2);
            availPositions.add(6);
            availPositions.add(7);
        }
        else if (beginLetterPos == 12){
            availPositions.add(8);
            availPositions.add(9);
            availPositions.add(13);
        }
        else if (beginLetterPos == 15){
            availPositions.add(14);
            availPositions.add(11);
            availPositions.add(10);
        }
        else if (beginLetterPos == 4 || beginLetterPos == 8){
            availPositions.add(beginLetterPos - 4);
            availPositions.add(beginLetterPos - 3);
            availPositions.add(beginLetterPos + 1);
            availPositions.add(beginLetterPos + 5);
            availPositions.add(beginLetterPos + 4);

        }
        else if (beginLetterPos == 7 || beginLetterPos == 11){
            availPositions.add(beginLetterPos - 4);
            availPositions.add(beginLetterPos - 5);
            availPositions.add(beginLetterPos - 1);
            availPositions.add(beginLetterPos + 3);
            availPositions.add(beginLetterPos + 4);

        }
        else if (beginLetterPos == 1 || beginLetterPos == 2){
            availPositions.add(beginLetterPos - 1);
            availPositions.add(beginLetterPos + 3);
            availPositions.add(beginLetterPos + 4);
            availPositions.add(beginLetterPos + 5);
            availPositions.add(beginLetterPos + 1);

        }
        else if (beginLetterPos == 13 || beginLetterPos == 14){
            availPositions.add(beginLetterPos - 1);
            availPositions.add(beginLetterPos - 5);
            availPositions.add(beginLetterPos - 4);
            availPositions.add(beginLetterPos - 3);
            availPositions.add(beginLetterPos + 1);

        }
        else {
            availPositions.add(beginLetterPos - 5);
            availPositions.add(beginLetterPos - 4);
            availPositions.add(beginLetterPos - 3);
            availPositions.add(beginLetterPos + 1);
            availPositions.add(beginLetterPos + 5);
            availPositions.add(beginLetterPos + 4);
            availPositions.add(beginLetterPos + 3);
            availPositions.add(beginLetterPos - 1);
        }

        int availPos[] = new int[availPositions.size()];

        for(int i = 0; i < availPos.length; i++){
            availPos[i] = availPositions.get(i);
        }

        return availPos;

    }

    public ArrayList<Button> getButtons(){
        return nodeButtons;
    }

    public ArrayList<Integer> getAvailPos(){
        return availPositions;
    }

    public void initSelect() {
        rowTwoSelect = new HBox();
        rowOneSelect = new HBox();
    }

    public BorderPane getFigurePane(){
        return figurePane;
    }

    public void disable(ArrayList<Button> buttons, Boolean disable) {
        for (int i = 0; i < buttons.size(); i++) {
            buttons.get(i).setDisable(disable);
        }
    }

    @Override
    public void reloadWorkspace() {

    }

    public void changeButtons() {
        pause.fire();
    }

    public int getTimesClicked() {
        return timesClicked;
    }

    public String getWord(int lvlSelect, boolean replay) {
        if (lvlSelect == 0) {     // then only 3 max for wordLength
            if(!replay) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Level 1!");
                alert.setContentText("You've selected level 1. " +
                        "That means minimum word length you can select is 3.");
                alert.showAndWait();
            }
            targetScore = 30;
            target.setText("Target: " + targetScore);
        }
        else if (lvlSelect == 1) {
            if(!replay) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Level 2!");
                alert.setContentText("You've selected level 2. " +
                        "That means minimum word length you can select is 4.");
                alert.showAndWait();
            }
            targetScore = 40;
            target.setText("Target: " + targetScore);
        }
        else if (lvlSelect == 2) {
            if(!replay) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Level 3!");
                alert.setContentText("You've selected level 3. " +
                        "That means minimum word length you can select is 5.");
                alert.showAndWait();
            }
            targetScore = 50;
            target.setText("Target: " + targetScore);
        }
        else if (lvlSelect == 3) {
            if(!replay) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Level 2!");
                alert.setContentText("You've selected level 4. " +
                        "That means minimum word length you can select is 6.");
                alert.showAndWait();
            }
            targetScore = 60;
            target.setText("Target: " + targetScore);
        }

        int randSize = possibleWords2.size();
        Random random = new Random();
        return possibleWords2.get(random.nextInt(randSize)); // returns a random word possible from our arraylist

    }

    // this will get all the possible words of the chosen length that can be put into the letter grid
    // use this method to later choose exactly one word to put into the grid
    public void getPossible(String mode, int number, int wordSize){
        possibleWords2 = new ArrayList<String>();
        solverList = new ArrayList<String>();
        URL wordsResource = getClass().getClassLoader().getResource("words/" + mode);
        assert wordsResource != null;

        // FILTERING PROCESS, WORDS WITH LETTERS ONLY //
        int toSkip = 0;

        while (toSkip < number) {
            try (Stream<String> lines = Files.lines(Paths.get(wordsResource.toURI()))) {
                String word = lines.skip(toSkip).findFirst().get();
                if(word.length() == wordSize+3) {
                    possibleWords2.add(word); // add word to arraylist
                }
                if(word.length() >= wordSize+3) {
                    possibleWords.add(word);      // add word to hashset
                    solverList.add(word);      // add word to solver's arraylist
                }
                toSkip++;                   // skip if neither of these

            } catch (IOException | URISyntaxException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
        return;
    }

    public AppTemplate getApp(){
        return app;
    }

    public void reinitBody(){
        bodyPane.getChildren().remove(figurePane);
        figurePane = new BorderPane();
        bodyPane.getChildren().add(figurePane);
    }

    public HBox getRowOneSelect() {
        return rowOneSelect;
    }

    public HBox getRowOne() {
        return rowOne;
    }
    public HBox getRowTwo() {
        return rowTwo;
    }
    public HBox getRowThree() {
        return rowThree;
    }
    public HBox getRowFour() {
        return rowFour;
    }

    public ArrayList getNodeButtons() {
        return nodeButtons;
    }
    public void initPauseButton(){
        pause = new Button();
    }

    public Button getPauseButton(){
        return pause;
    }
    public void setTimesClicked(int timesClicked){
        this.timesClicked = timesClicked;
    }

    public Label getTarget() {
        return target;
    }

    public ArrayList<String> getSolverList() {
        return solverList;
    }
    public ArrayList<String> getPossibleWords2() {
        return possibleWords2;
    }
    public int getTargetScore(){
        return targetScore;
    }

}