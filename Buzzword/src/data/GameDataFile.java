package data;

import com.fasterxml.jackson.core.*;

import components.AppDataComponent;
import components.AppFileComponent;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

/*
@author Christopher Wong

 */

public class GameDataFile implements AppFileComponent {
    public static final String USERNAME  = "USERNAME";
    public static final String PASSWORD = "PASSWORD";
    public static final String LEVELS_UNLOCKED  = "LEVELS_UNLOCKED";
    public static final String PERSONAL_BEST = "PERSONAL_BEST";

    @Override
    public void saveData(AppDataComponent data, Path filePath) throws IOException {
        GameData       gamedata    = (GameData) data;
        String username = gamedata.getUsername();
        String password = gamedata.getPassword();
        int[] levelsUnlocked = gamedata.getLevelsUnlocked();
        int personalBest = gamedata.getPersonalBest();

        JsonFactory jsonFactory = new JsonFactory();

        try (OutputStream out = Files.newOutputStream(filePath)) {

            JsonGenerator generator = jsonFactory.createGenerator(out, JsonEncoding.UTF8);

            generator.writeStartObject();

            generator.writeStringField(USERNAME, username);

            generator.writeEndObject();

            generator.writeStartObject();

            generator.writeStringField(PASSWORD, password);

            generator.writeEndObject();

            generator.writeStartObject();
            generator.writeFieldName(LEVELS_UNLOCKED);
            generator.writeStartArray(3);
            for (int i = 0; i < 3; i++)
                generator.writeNumber(levelsUnlocked[i]);
            generator.writeEndArray();

            generator.writeEndObject();

            generator.writeStartObject();
            generator.writeFieldName(PERSONAL_BEST);
            generator.writeNumber(personalBest);
            generator.writeEndObject();

            generator.close();

        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    @Override
    public void loadData(AppDataComponent data, Path filePath) throws IOException {

    }

    @Override
    public void exportData(AppDataComponent data, Path filePath) throws IOException {

    }
}