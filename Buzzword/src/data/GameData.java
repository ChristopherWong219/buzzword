package data;

import apptemplate.AppTemplate;
import components.AppDataComponent;

/*
@author Christopher Wong

 */

// we want to save the user's username, password, levelsUnlocked
public class GameData implements AppDataComponent {
    private String username;
    private String password;
    private int[] levelsUnlocked;
    private int personalBest;

    public GameData(AppTemplate appTemplate) {
    }

    public GameData(AppTemplate appTemplate, boolean initiateGame) {

    }

    @Override
    public void reset(){
    }

    public void init(){
        this.username = "";
        this.password = "";
        this.levelsUnlocked = new int[3];
    }

    public String getUsername(){
        return username;
    }
    public String getPassword(){
        return password;
    }
    public int[] getLevelsUnlocked(){
        return levelsUnlocked;
    }
    public int getPersonalBest(){ return personalBest;}

    public void setUsername(String username){
        this.username = username;
    }
    public void setPassword(String password){
        this.password = password;
    }
    public void setLevelsUnlocked(int[] levelsUnlocked){
        this.levelsUnlocked = levelsUnlocked;
    }
    public void setPersonalBest(int personalBest){
        this.personalBest = personalBest;
    }
}
