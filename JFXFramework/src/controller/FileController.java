package controller;

import java.io.IOException;

/**
 * @author Ritwik Banerjee
 */
public interface FileController {

    void handleNewRequest();

    void handleSaveRequest() throws IOException;

    void handleLoadRequest() throws IOException;

    void handleExitRequest();

    void handleGameExitRequest();

    void handleCreateRequest();

    void handleLoginRequest();

    void handleComboRequest();

    void handleStartRequest();

    void handleViewProfile();

    void handleHomeRequest();

    void handleHomeGamePlayRequest();

    void handleHelpRequest();

    void handleControlRequest();
}
