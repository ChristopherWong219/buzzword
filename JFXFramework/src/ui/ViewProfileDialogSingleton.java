package ui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * @author Ritwik Banerjee
 */
public class ViewProfileDialogSingleton extends Stage {
    // HERE'S THE SINGLETON
    static ViewProfileDialogSingleton singleton;

    // GUI CONTROLS FOR OUR DIALOG
    VBox   messagePane;
    Scene  messageScene;
    Label  messageLabel;
    Button signOutButton;
    Button editProfileButton;
    Button cancelButton;
    String selection;

    // CONSTANT CHOICES
    public static final String LOGOUT    = "Logout";
    public static final String EDIT      = "Edit";
    public static final String CANCEL    = "Cancel";


    /**
     * Note that the constructor is private since it follows
     * the singleton design pattern.
     */
    private ViewProfileDialogSingleton() {}

    /**
     * The static accessor method for this singleton.
     *
     * @return The singleton object for this type.
     */
    public static ViewProfileDialogSingleton getSingleton() {
        if (singleton == null)
            singleton = new ViewProfileDialogSingleton();
        return singleton;
    }

    /**
     * This method initializes the singleton for use.
     *
     * @param primaryStage The window above which this dialog will be centered.
     */
    public void init(Stage primaryStage) {
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        // LABEL TO DISPLAY THE CUSTOM MESSAGE
        messageLabel = new Label();
        messageLabel.setAlignment(Pos.BOTTOM_CENTER);

        signOutButton = new Button(LOGOUT);
        editProfileButton = new Button(EDIT);
        cancelButton = new Button(CANCEL);

        EventHandler<KeyEvent> esc = new EventHandler<KeyEvent>(){
            @Override
            public void handle(KeyEvent e){
                if(e.getCode() == KeyCode.ESCAPE){
                    selection = CANCEL;
                    ViewProfileDialogSingleton.this.close();
                }
            }
        };

        // MAKE THE EVENT HANDLER FOR THESE BUTTONS
        EventHandler<ActionEvent> finish = event -> {
            selection = LOGOUT;
            this.close();

        };

        EventHandler<ActionEvent> edit = event -> {
            selection = EDIT;
            this.close();

        };

        EventHandler<ActionEvent> cancel = event -> {
            selection = CANCEL;
            this.close();

        };


        // AND THEN REGISTER THEM TO RESPOND TO INTERACTIONS
        signOutButton.setOnAction(finish);
        editProfileButton.setOnAction(edit);
        cancelButton.setOnAction(cancel);

        // NOW ORGANIZE OUR BUTTONS
        HBox buttonBox = new HBox();
        buttonBox.getChildren().add(signOutButton);
        buttonBox.getChildren().add(editProfileButton);
        buttonBox.getChildren().add(cancelButton);

        // WE'LL PUT EVERYTHING HERE
        messagePane = new VBox();
        messagePane.setAlignment(Pos.CENTER);
        messagePane.getChildren().add(messageLabel);
        messagePane.getChildren().add(buttonBox);

        // MAKE IT LOOK NICE
        messagePane.setPadding(new Insets(10, 130, 130, 20));
        messagePane.setSpacing(10);

        // AND PUT IT IN THE WINDOW
        messageScene = new Scene(messagePane);
        messageScene.setOnKeyPressed(esc);
        this.setScene(messageScene);
        this.initStyle(StageStyle.UNDECORATED);

    }

    /**
     * Accessor method for getting the selection the user made.
     *
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }


    /**
     * This method loads a custom message into the label
     * then pops open the dialog.
     *
     * @param title   The title to appear in the dialog window bar.
     * @param message Message to appear inside the dialog.
     */
    public void show(String title, String message) {
        // SET THE DIALOG TITLE BAR TITLE
        setTitle(title);

        // SET THE MESSAGE TO DISPLAY TO THE USER
        messageLabel.setText(message);

        // AND OPEN UP THIS DIALOG, MAKING SURE THE APPLICATION
        // WAITS FOR IT TO BE RESOLVED BEFORE LETTING THE USER
        // DO MORE WORK.
        showAndWait();
    }

}
