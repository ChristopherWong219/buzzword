package ui;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import propertymanager.PropertyManager;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;

import static settings.AppPropertyType.APP_TITLE;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * @author Christopher Wong
 */
public class LoginProfileDialogSingleton extends Stage {
    // HERE'S THE SINGLETON
    static LoginProfileDialogSingleton singleton;

    // GUI CONTROLS FOR OUR DIALOG
    VBox   messagePane;
    Scene  messageScene;
    Label  messageLabel;
    TextField username;
    PasswordField password;
    VBox usernameStuff;
    VBox passwordStuff;
    Button yesButton;
    Button cancelButton;
    String selection;
    HBox userError;
    HBox passError;
    Label userErrorLab;
    Label passErrorLab;
    String password1;
    ArrayList<Integer> unlocked = new ArrayList<Integer>();
    int personalBest;

    // data
    String usernameData;

    // CONSTANT CHOICES
    public static final String FINISH    = "Login";
    public static final String CANCEL = "Cancel";
    private boolean exists;
    private boolean correctPass;
    String passwordData;

    /**
     * Note that the constructor is private since it follows
     * the singleton design pattern.
     */
    private LoginProfileDialogSingleton() {}

    /**
     * The static accessor method for this singleton.
     *
     * @return The singleton object for this type.
     */
    public static LoginProfileDialogSingleton getSingleton() {
        if (singleton == null)
            singleton = new LoginProfileDialogSingleton();
        return singleton;
    }

    /**
     * This method initializes the singleton for use.
     *
     * @param primaryStage The window above which this dialog will be centered.
     */
    public void init(Stage primaryStage) {
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);

        // LABEL TO DISPLAY THE CUSTOM MESSAGE
        messageLabel = new Label();

        // YES, NO, AND CANCEL BUTTONS
        username = new TextField();
        password = new PasswordField();

        username.setPrefSize(100, 10);
        password.setPrefSize(100, 10);

        // hboxes
        usernameStuff = new VBox();
        passwordStuff = new VBox();

        userError = new HBox();
        passError = new HBox();

        userErrorLab = new Label("                  ");
        passErrorLab = new Label("                  ");


        userError.getChildren().addAll(username, userErrorLab);
        passError.getChildren().addAll(password, passErrorLab);

        Label usernameLab, passwordLab;
        usernameLab = new Label("Username: ");
        passwordLab = new Label("Password:  ");

        usernameStuff.getChildren().addAll(usernameLab, userError);
        passwordStuff.getChildren().addAll(passwordLab, passError);

        yesButton = new Button(FINISH);
        cancelButton = new Button(CANCEL);

        EventHandler<KeyEvent> esc = new EventHandler<KeyEvent>(){
            @Override
            public void handle(KeyEvent e){
                if(e.getCode() == KeyCode.ESCAPE){
                    username.setText("");
                    password.setText("");
                    selection = CANCEL;
                    LoginProfileDialogSingleton.this.close();
                }
                if(e.getCode() == KeyCode.ENTER){
                    LoginProfileDialogSingleton.this.yesButton.fire();
                }
            }
        };

        // MAKE THE EVENT HANDLER FOR THESE BUTTONS
        EventHandler<ActionEvent> yesNoCancelHandler = event -> {
            correctPass = false;
            selection = FINISH;
            exists = nameExists(username.getText());
            if(exists){
                try {
                    correctPass = correctPass(username.getText(), password.getText());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(!exists || !correctPass){
                if(!exists) {
                    userErrorLab.setText(" Doesn't exist!");
                    userErrorLab.setTextFill(Paint.valueOf("RED"));
                }
                if(!correctPass){
                    passErrorLab.setText(" Incorrect!");
                    passErrorLab.setTextFill(Paint.valueOf("RED"));
                }
                if(!correctPass && exists){
                    userErrorLab.setText("              ");
                }
            }
            else {
                usernameData = username.getText();
                passwordData = password.getText();

                username.setText("");
                password.setText("");
                userErrorLab.setText("           ");
                passErrorLab.setText("           ");

                this.close();
            }
        };

        EventHandler<ActionEvent> cancelHandler = event -> {
            selection = CANCEL;

            username.setText("");
            password.setText("");
            userErrorLab.setText("           ");
            passErrorLab.setText("           ");
            this.close();
        };


        // AND THEN REGISTER THEM TO RESPOND TO INTERACTIONS
        yesButton.setOnAction(yesNoCancelHandler);
        cancelButton.setOnAction(cancelHandler);

        // NOW ORGANIZE OUR BUTTONS
        HBox buttonBox = new HBox();
        buttonBox.getChildren().add(yesButton);
        buttonBox.getChildren().add(cancelButton);

        VBox textfields = new VBox();
        textfields.getChildren().addAll(usernameStuff, passwordStuff);

        // WE'LL PUT EVERYTHING HERE
        messagePane = new VBox();
        messagePane.setAlignment(Pos.CENTER);
        messagePane.getChildren().add(messageLabel);
        messagePane.getChildren().add(textfields);
        messagePane.getChildren().add(buttonBox);

        // MAKE IT LOOK NICE
        messagePane.setPadding(new Insets(10, 130, 130, 20));
        messagePane.setSpacing(10);

        // AND PUT IT IN THE WINDOW
        messageScene = new Scene(messagePane);
        messageScene.setOnKeyPressed(esc);
        this.setScene(messageScene);
        this.initStyle(StageStyle.UNDECORATED);

    }

    /**
     * Accessor method for getting the selection the user made.
     *
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }

    public String getUsernameData(){ return usernameData; }
    public String getPasswordData(){ return passwordData; }

    private boolean nameExists(String username) {
        PropertyManager propertyManager = PropertyManager.getManager();
        Path appDirPath      = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
        Path targetPath      = appDirPath.resolve(APP_WORKDIR_PATH.getParameter() + "/" + username + ".json");
        File checkFile = targetPath.toFile();

        return checkFile.exists();
    }

    private boolean correctPass(String username, String password) throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        Path appDirPath      = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
        Path targetPath      = appDirPath.resolve(APP_WORKDIR_PATH.getParameter() + "/" + username + ".json");

        JsonFactory jsonFactory = new JsonFactory();
        JsonParser  jsonParser  = jsonFactory.createParser(Files.newInputStream(targetPath));

        while (!jsonParser.isClosed()) {
            JsonToken token = jsonParser.nextToken();
            if (JsonToken.FIELD_NAME.equals(token)) {
                String fieldname = jsonParser.getCurrentName();
                switch (fieldname) {
                    case "USERNAME":
                        jsonParser.nextToken();
                        break;
                    case "PASSWORD":
                        jsonParser.nextToken();
                        password1 = jsonParser.getValueAsString();
                        break;
                    case "LEVELS_UNLOCKED":
                        jsonParser.nextToken();
                        unlocked.clear();
                        while (jsonParser.nextToken() != JsonToken.END_ARRAY)
                            unlocked.add(jsonParser.getValueAsInt());
                        break;
                    case "PERSONAL_BEST":
                        jsonParser.nextToken();
                        personalBest = jsonParser.getValueAsInt();
                        break;
                    default:
                        throw new JsonParseException(jsonParser, "Unable to load JSON data");
                }
            }
        }

        final String decoded = new String(Base64.getDecoder().decode(password1));

        boolean equal = password.equals(decoded);
        return equal;
    }

    public int[] getUnlocked(){
        int[] array = new int [3];
        array[0] = unlocked.get(0);
        array[1] = unlocked.get(1);
        array[2] = unlocked.get(2);

        return array;
    }

    public int getPersonalBest(){
        return personalBest;
    }


    /**
     * This method loads a custom message into the label
     * then pops open the dialog.
     *
     * @param title   The title to appear in the dialog window bar.
     * @param message Message to appear inside the dialog.
     */
    public void show(String title, String message) {
        // SET THE DIALOG TITLE BAR TITLE
        setTitle(title);

        // SET THE MESSAGE TO DISPLAY TO THE USER
        messageLabel.setText(message);

        // AND OPEN UP THIS DIALOG, MAKING SURE THE APPLICATION
        // WAITS FOR IT TO BE RESOLVED BEFORE LETTING THE USER
        // DO MORE WORK.
        showAndWait();
    }

}
