package ui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import propertymanager.PropertyManager;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import static settings.AppPropertyType.APP_TITLE;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * @author Ritwik Banerjee
 */
public class EditProfileDialogSingleton extends Stage {
    // HERE'S THE SINGLETON
    static EditProfileDialogSingleton singleton;

    // GUI CONTROLS FOR OUR DIALOG
    VBox   messagePane;
    Scene  messageScene;
    Label  messageLabel;
    TextField username;
    PasswordField password;
    PasswordField confirm;
    VBox usernameStuff;
    VBox passwordStuff;
    VBox confirmStuff;
    Button yesButton;
    Button cancelButton;
    String selection;
    Text user;
    Text pass;
    Text confirmPass;
    HBox userError;
    HBox passError;
    HBox confirmError;
    boolean exists;

    // CONSTANT CHOICES
    public static final String FINISH    = "Edit";
    public static final String CANCEL = "Cancel";

    /**
     * Note that the constructor is private since it follows
     * the singleton design pattern.
     */
    private EditProfileDialogSingleton() {}

    /**
     * The static accessor method for this singleton.
     *
     * @return The singleton object for this type.
     */
    public static EditProfileDialogSingleton getSingleton() {
        if (singleton == null)
            singleton = new EditProfileDialogSingleton();
        return singleton;
    }

    /**
     * This method initializes the singleton for use.
     *
     * @param primaryStage The window above which this dialog will be centered.
     */
    public void init(Stage primaryStage) {
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);

        userError = new HBox();
        passError = new HBox();
        confirmError = new HBox();

        HBox.setHgrow(userError, Priority.NEVER);
        HBox.setHgrow(passError, Priority.NEVER);
        HBox.setHgrow(confirmError, Priority.NEVER);

        user = new Text();
        pass = new Text();
        confirmPass = new Text();

        user.setText("                          ");
        pass.setText("                          ");
        confirmPass.setText("                           ");

        user.setFill(Paint.valueOf("RED"));
        pass.setFill(Paint.valueOf("RED"));
        confirmPass.setFill(Paint.valueOf("RED"));

        // LABEL TO DISPLAY THE CUSTOM MESSAGE
        messageLabel = new Label();

        // YES, NO, AND CANCEL BUTTONS
        username = new TextField();
        password = new PasswordField();
        confirm = new PasswordField();

        username.setAlignment(Pos.CENTER_LEFT);
        password.setAlignment(Pos.CENTER_LEFT);
        confirm.setAlignment(Pos.CENTER_LEFT);

        username.setMinSize(150, 10);
        password.setMinSize(150, 10);
        confirm.setMinSize(150, 10);

        // hboxes
        usernameStuff = new VBox();
        passwordStuff = new VBox();
        confirmStuff = new VBox();

        Label usernameLab, passwordLab, confirmLab;
        usernameLab = new Label("Change Username: ");
        passwordLab = new Label("Change Password:  ");
        confirmLab = new Label("Confirm new password: ");

        userError.getChildren().addAll(username, user);
        passError.getChildren().addAll(password, pass);
        confirmError.getChildren().addAll(confirm, confirmPass);

        usernameStuff.getChildren().addAll(usernameLab, userError);
        passwordStuff.getChildren().addAll(passwordLab, passError);
        confirmStuff.getChildren().addAll(confirmLab, confirmError);

        yesButton = new Button(FINISH);
        cancelButton = new Button(CANCEL);

        EventHandler<KeyEvent> esc = new EventHandler<KeyEvent>(){
            @Override
            public void handle(KeyEvent e){
                if(e.getCode() == KeyCode.ESCAPE){
                    username.setText("");
                    password.setText("");
                    confirm.setText("");
                    selection = "";
                    EditProfileDialogSingleton.this.close();
                }
                if(e.getCode() == KeyCode.ENTER){
                    EditProfileDialogSingleton.this.yesButton.fire();
                }
            }
        };

        // MAKE THE EVENT HANDLER FOR THESE BUTTONS
        EventHandler<ActionEvent> finish = event -> {
            exists = nameExists(getUsername());

            user.setText("                          ");
            pass.setText("                          ");
            confirmPass.setText("                           ");

            this.selection = FINISH;
            if(getUsername().length() < 3 || exists || getPassword().length()<8 || getConfirm().length()<8 ||
                    !getPassword().equals(getConfirm())){
                if(getUsername().length()<3){
                    user.setText(" Min. 3 chars!");
                }
                if(exists){
                    user.setText(" Already exists!");
                }
                if(getPassword().length() < 8){
                    pass.setText(" Min. 8 chars!");
                }
                if(getConfirm().length() < 8 || !getPassword().equals(getConfirm())){
                    confirmPass.setText(" Invalid.");
                }
            }

            else {
                this.close();
            }
        };

        EventHandler<ActionEvent> cancel = event -> {
            user.setText("                          ");
            pass.setText("                          ");
            confirmPass.setText("                        ");
            username.setText("");
            password.setText("");
            confirm.setText("");
            this.selection = CANCEL;
            this.hide();

        };


        // AND THEN REGISTER THEM TO RESPOND TO INTERACTIONS
        yesButton.setOnAction(finish);
        cancelButton.setOnAction(cancel);

        // NOW ORGANIZE OUR BUTTONS
        HBox buttonBox = new HBox();
        buttonBox.getChildren().add(yesButton);
        buttonBox.getChildren().add(cancelButton);

        VBox textfields = new VBox();
        textfields.getChildren().addAll(usernameStuff, passwordStuff, confirmStuff);

        // WE'LL PUT EVERYTHING HERE
        messagePane = new VBox();
        messagePane.setAlignment(Pos.CENTER);
        messagePane.getChildren().add(messageLabel);
        messagePane.getChildren().add(textfields);
        messagePane.getChildren().add(buttonBox);

        // MAKE IT LOOK NICE
        messagePane.setPadding(new Insets(10, 130, 130, 20));
        messagePane.setSpacing(10);

        // AND PUT IT IN THE WINDOW
        messageScene = new Scene(messagePane);
        messageScene.setOnKeyPressed(esc);
        this.setScene(messageScene);
        this.initStyle(StageStyle.UNDECORATED);

    }

    private boolean nameExists(String username) {
        PropertyManager propertyManager = PropertyManager.getManager();
        Path            appDirPath      = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
        Path targetPath      = appDirPath.resolve(APP_WORKDIR_PATH.getParameter() + "/" + username + ".json");
        File checkFile = targetPath.toFile();

        return checkFile.exists();
    }

    /**
     * Accessor method for getting the selection the user made.
     *
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }


    /**
     * This method loads a custom message into the label
     * then pops open the dialog.
     *
     * @param title   The title to appear in the dialog window bar.
     * @param message Message to appear inside the dialog.
     */
    public void show(String title, String message) {
        // SET THE DIALOG TITLE BAR TITLE
        setTitle(title);

        // SET THE MESSAGE TO DISPLAY TO THE USER
        messageLabel.setText(message);

        // AND OPEN UP THIS DIALOG, MAKING SURE THE APPLICATION
        // WAITS FOR IT TO BE RESOLVED BEFORE LETTING THE USER
        // DO MORE WORK.
        showAndWait();
    }

    public String getUsername(){
        return username.getText();
    }
    public String getPassword(){
        return password.getText();
    }
    public String getConfirm(){
        return confirm.getText();
    }

    public void reset(){
        username.setText("");
        password.setText("");
        confirm.setText("");
    }

}
