package ui;

import apptemplate.AppTemplate;
import components.AppStyleArbiter;
import controller.FileController;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import propertymanager.PropertyManager;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_IMAGEDIR_PATH;

/**
 * This class provides the basic user interface for this application, including all the file controls, but it does not
 * include the workspace, which should be customizable and application dependent.
 *
 * @author Richard McKenna, Ritwik Banerjee, Christopher Wong
 */
public class AppGUI implements AppStyleArbiter {

    protected FileController fileController;   // to react to file-related controls
    protected Stage          primaryStage;     // the application window
    protected Scene          primaryScene;     // the scene graph
    protected BorderPane     appPane;          // the root node in the scene graph, to organize the containers
    protected BorderPane     toolbarPane;      // the top toolbar
    protected Button         createButton;     // button to create a new instance of the application
    protected Button         loginButton;       // button to load a saved game from (json) file
    protected Button         exitButton;       // button to quit app
    protected String         applicationTitle; // the application title
    protected VBox           sideBar;          // to display the side controls
    protected FlowPane       sideBarPane;      // the side toolbar
    protected FlowPane       mainGamePane;     // this is where all the game action happens
    protected ComboBox<String> comboBox;       // our choices for game mode
    protected Button         profButton;       // profile button
    protected Button         startPlaying;     // start the game play
    protected Button         home;             // go back home screen
    protected Button         help;             // help button

    private int appWindowWidth;  // optional parameter for window width that can be set by the application
    private int appWindowHeight; // optional parameter for window height that can be set by the application
    private String selected;     // selected from combobox

    /**
     * This constructor initializes the file toolbar for use.
     *
     * @param initPrimaryStage The window for this application.
     * @param initAppTitle     The title of this application, which
     *                         will appear in the window bar.
     * @param app              The app within this gui is used.
     */
    public AppGUI(Stage initPrimaryStage, String initAppTitle, AppTemplate app) throws IOException, InstantiationException {
        this(initPrimaryStage, initAppTitle, app, -1, -1);
    }

    public AppGUI(Stage primaryStage, String applicationTitle, AppTemplate appTemplate, int appWindowWidth, int appWindowHeight) throws IOException, InstantiationException {
        this.appWindowWidth = appWindowWidth;
        this.appWindowHeight = appWindowHeight;
        this.primaryStage = primaryStage;
        this.applicationTitle = applicationTitle;
        initializeBars();                    // initialize the side toolbar
        initializeBarHandlers(appTemplate); // set the toolbar button handlers
        initializeWindow();                     // start the app window (without the application-specific workspace)

    }

    public FileController getFileController() {
        return this.fileController;
    }

    public BorderPane getToolbarPane() { return toolbarPane; }

    public FlowPane getSideBarPane() { return sideBarPane; }

    public FlowPane getMainGamePane() { return mainGamePane; }

    public BorderPane getAppPane() { return appPane; }

    public void setProfButton(String profButton) { this.profButton.setText(profButton); }

    /**
     * Accessor method for getting this application's primary stage's,
     * scene.
     *
     * @return This application's window's scene.
     */
    public Scene getPrimaryScene() { return primaryScene; }

    /**
     * Accessor method for getting this application's window,
     * which is the primary stage within which the full GUI will be placed.
     *
     * @return This application's primary stage (i.e. window).
     */
    public Stage getWindow() { return primaryStage; }

    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    public void initializeBars() throws IOException {
        toolbarPane = new BorderPane();
        sideBarPane = new FlowPane();
        mainGamePane = new FlowPane();
        toolbarPane.setPrefHeight(100);
        sideBarPane.setPrefWidth(150);
        sideBar = new VBox(1);
        createButton = initializeChildButton(sideBar, "Create Profile", CREATE_TOOLTIP.toString(), false, false);
        loginButton = initializeChildButton(sideBar, "Login", LOGIN_TOOLTIP.toString(), false, false);
        help = initializeChildButton(sideBar,"Help", HELP_TOOLTIP.toString(), false, false);
        exitButton = initializeChildButton(toolbarPane,EXIT_ICON.toString(), EXIT_TOOLTIP.toString(), false, true);
        sideBarPane.getChildren().addAll(sideBar);
    }

    public void transitionFromLogin(){
        sideBar.getChildren().remove(createButton);
        sideBar.getChildren().remove(loginButton);
        sideBar.getChildren().remove(help);
    }

    public void addProfButton(String username){

        comboBox = new ComboBox<String>();
        comboBox.setValue("     Select Mode");
        comboBox.getItems().addAll("Food", "Christmas", "Fitness");
        PropertyManager propertyManager = PropertyManager.getManager();
        Tooltip buttonTooltip = new Tooltip(propertyManager.getPropertyValue(MODE_TOOLTIP.toString()));
        comboBox.setTooltip(buttonTooltip);
        comboBox.setPrefSize(150, 25);

        profButton = initializeChildButton(sideBar, username, PROFILE_TOOLTIP.toString(), false, false);
        profButton.setOnAction(e -> fileController.handleViewProfile());
        sideBar.getChildren().add(comboBox);
        startPlaying = initializeChildButton(sideBar,"Start Game", STARTGAME_TOOLTIP.toString(), false, false);
        startPlaying.setOnAction(e->fileController.handleStartRequest());
        sideBar.getChildren().add(help);

    }

    public Button initializeChildButton(Pane bar, String text, String tooltip, boolean disabled, boolean img) {
        PropertyManager propertyManager = PropertyManager.getManager();
        Button button;
        if (img){
            URL imgDirURL = AppTemplate.class.getClassLoader().getResource(APP_IMAGEDIR_PATH.getParameter());
            if (imgDirURL == null)
                try {
                    throw new FileNotFoundException("Image resources folder does not exist.");
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

            button = new Button();
            try {
                try (InputStream imgInputStream = Files.newInputStream(Paths.get(imgDirURL.toURI()).resolve(propertyManager.getPropertyValue(text)))) {
                    Image buttonImage = new Image(imgInputStream);
                    button.setDisable(disabled);
                    button.setGraphic(new ImageView(buttonImage));
                    Tooltip buttonTooltip = new Tooltip(propertyManager.getPropertyValue(tooltip));
                    button.setTooltip(buttonTooltip);
                    ((BorderPane)bar).setRight(button);
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    System.exit(1);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            button = new Button();
            button.setPrefSize(150, 25);
            button.setText(text);
            button.setDisable(disabled);
            Tooltip buttonTooltip = new Tooltip(propertyManager.getPropertyValue(tooltip));
            button.setTooltip(buttonTooltip);
            bar.getChildren().add(button);
        }

        return button;
    }

    public void initializeBarHandlers(AppTemplate app) throws InstantiationException {
        try {
            Method getFileControllerClassMethod = app.getClass().getMethod("getFileControllerClass");
            String fileControllerClassName = (String) getFileControllerClassMethod.invoke(app);
            Class<?> klass = Class.forName("controller." + fileControllerClassName);
            Constructor<?> constructor = klass.getConstructor(AppTemplate.class);
            fileController = (FileController) constructor.newInstance(app);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException | ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        }
        createButton.setOnAction(e -> fileController.handleCreateRequest());
        loginButton.setOnAction(e -> fileController.handleLoginRequest());
        help.setOnAction(e -> fileController.handleHelpRequest());
        exitButton.setOnAction(e -> fileController.handleExitRequest());

    }

    public void exitButtonGamePlay(){
        exitButton.setOnAction(e -> fileController.handleGameExitRequest());
    }

    public void exitHomeRegular() { home.setOnAction(e -> fileController.handleHomeRequest());}

    public void exitHomeGamePlay(){ home.setOnAction(e -> fileController.handleHomeGamePlayRequest());
    }

    public String getSelectionCombo(){
        String selected = "";
        selected = comboBox.getSelectionModel().getSelectedItem();

        return selected;
    }

    public void initializeWindow() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();

        // SET THE WINDOW TITLE
        primaryStage.setTitle(applicationTitle);
        primaryStage.initStyle(StageStyle.UNDECORATED);

        // add the toolbar to the constructed workspace
        appPane = new BorderPane();
        appPane.setTop(toolbarPane);
        appPane.setLeft(sideBarPane);
        appPane.setCenter(mainGamePane);
        primaryScene = appWindowWidth < 1 || appWindowHeight < 1 ? new Scene(appPane)
                : new Scene(appPane,
                appWindowWidth,
                appWindowHeight);

        URL imgDirURL = AppTemplate.class.getClassLoader().getResource(APP_IMAGEDIR_PATH.getParameter());
        if (imgDirURL == null)
            throw new FileNotFoundException("Image resources folder does not exist.");
        try (InputStream appLogoStream = Files.newInputStream(Paths.get(imgDirURL.toURI()).resolve(propertyManager.getPropertyValue(APP_LOGO)))) {
            primaryStage.getIcons().add(new Image(appLogoStream));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }

    public Stage getPrimaryStage(){
        return primaryStage;
    }

    public void reinitializeWindow() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();

        // add the toolbar to the constructed workspace
        appPane.setTop(toolbarPane);
        appPane.setLeft(sideBarPane);


        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }

    /**
     * This is a public helper method for initializing a simple button with
     * an icon and tooltip and placing it into a toolbar.
     *
     * @param sideBarPane Sidebar pane into which to place this button.
     * @param icon        Icon image file name for the button.
     * @param tooltip     Tooltip to appear when the user mouses over the button.
     * @param disabled    true if the button is to start off disabled, false otherwise.
     * @return A constructed, fully initialized button placed into its appropriate
     * pane container.
     */


    /**
     * This function specifies the CSS style classes for the controls managed
     * by this framework.
     */
    @Override
    public void initStyle() {
        // currently, we do not provide any stylization at the framework-level
    }

    public String getCombo() {
        selected = getSelectionCombo();

        return selected;
    }

    public ComboBox<String> getComboBox() {
        return comboBox;
    }

    public Button getStartButton(){
        return startPlaying;
    }

    public void refreshSidebar(){
        sideBar.getChildren().clear();
    }

    public void transitionFromCombo(){
            sideBar.getChildren().remove(comboBox);
            sideBar.getChildren().remove(startPlaying);

    }

    public void createSelectionScreen() {
        home = initializeChildButton(sideBar, "Home", HOME_TOOLTIP.toString(), false, false);
    }

    public void removeHelp() {
        sideBar.getChildren().remove(help);
    }

    public void removeCombo(){
        sideBar.getChildren().remove(comboBox);
    }
    public void removeStart(){
        sideBar.getChildren().remove(startPlaying);
    }

    public void addHome() {
        sideBar.getChildren().add(home);
    }
}