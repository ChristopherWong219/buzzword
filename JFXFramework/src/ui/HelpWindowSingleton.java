package ui;

import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Created by C on 12/11/2016.
 */
public class HelpWindowSingleton extends Stage {
    private static HelpWindowSingleton singleton = null;
    private Label messageLabel = new Label("hi");
    VBox vBox = new VBox();

    private HelpWindowSingleton() {

    }

    public static HelpWindowSingleton getSingleton() {
        if (singleton == null) {
            singleton = new HelpWindowSingleton();
        }
        return singleton;
    }

    public void init(Stage owner) {
        initModality(Modality.WINDOW_MODAL); // modal => messages are blocked from reaching other windows
        initOwner(owner);

//        Button closeButton = new Button(InitializationParameters.CLOSE_LABEL.getParameter());
//        closeButton.setOnAction(e -> this.close());

        EventHandler<KeyEvent> esc = new EventHandler<KeyEvent>(){
            @Override
            public void handle(KeyEvent e){
                if(e.getCode() == KeyCode.ESCAPE){

                    HelpWindowSingleton.this.close();
                }
                if(e.getCode() == KeyCode.ENTER){
                    HelpWindowSingleton.this.close();
                }

            }
        };


        ScrollPane sp = new ScrollPane();
        sp.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        Image help = new Image(getClass().getResourceAsStream("/images/Help.png"));
        sp.setContent(new ImageView(help));
        sp.setMaxSize(1000,600);

        Scene messageScene = new Scene(sp);
        messageScene.setOnKeyPressed(esc);
        this.setScene(messageScene);
        this.initStyle(StageStyle.UNDECORATED);
    }

}
